// Usage:
// cd backend
// node src/scripts/test.js Creates test courses in the database 

const io = require('socket.io-client')
const feathers = require('@feathersjs/feathers')
const socketio = require('@feathersjs/socketio-client')

const socket = io('http://localhost:3030')
const app = feathers()
app.configure(socketio(socket))

async function main() {
    await app.service('cours').create({"name": "Python", "date": "2022-01-12" })
    await app.service('cours').create({"name": "Java", "date": "2022-01-18" })
    await app.service('cours').create({"name": "Cuisine", "date": "2022-01-01" })
    await app.service('cours').create({"name": "Kung-Fu", "date": "2022-01-22" })
    await app.service('cours').create({"name": "VueJS", "date": "2022-01-27" })
    await app.service('cours').create({"name": "Danse interprétative", "date": "2022-01-30" })
    await app.service('cours').create({"name": "Algo", "date": "2022-01-14" })
    await app.service('cours').create({"name": "Café", "date": "2022-01-11" })
    await app.service('cours').create({"name": "Rumba", "date": "2022-01-29" })
    const listeCours = await app.service('cours').find({})
    console.log(listeCours)


//    console.log("cours: ", cours)
   // const notes = await app.service('notes').find({id: 26})
   // console.log("notes: ", notes)
    
   process.exit(0)
}

main()