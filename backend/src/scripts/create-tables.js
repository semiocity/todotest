
const feathers = require('@feathersjs/feathers')
const configuration = require('@feathersjs/configuration')

const knex = require('../knex')

const createCoursTable = require('../services/cours/cours.model')


async function main() {
   const app = feathers()

   app.configure(configuration())
   app.configure(knex)

   // database   
   await createCoursTable(app, "cours")

   process.exit(0)
}

main()
