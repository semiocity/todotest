// Usage:
// cd backend
// node src/scripts/test.js Creates test courses in the database 

const io = require('socket.io-client')
const feathers = require('@feathersjs/feathers')
const socketio = require('@feathersjs/socketio-client')

const socket = io('http://localhost:3030')
const app = feathers()
app.configure(socketio(socket))

async function main() {
   const cours = await app.service('cours').create({"name": "Python", "date": "2022-01-10" })
   console.log("cours: ", cours)
   // const notes = await app.service('notes').find({id: 26})
   // console.log("notes: ", notes)

   process.exit(0)
}

main()