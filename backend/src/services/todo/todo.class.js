const { Service } = require('feathers-knex');

exports.Todo = class Todo extends Service {
  constructor(options) {
    super({
      ...options,
      name: 'todo'
    });
  }
};
