
// database
const todoService = require('./todo/todo.service.js')

module.exports = function (app) {
   app.configure(todoService)
}
