import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './css/tailwind.css'

// Setup plugin for defaults or `$screens` (optional)
createApp(App)
  .use(router)
  .mount('#app')
