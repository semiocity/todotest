import io from 'socket.io-client'
import feathers from '@feathersjs/client'

// Create a websocket connecting to Feathers server
// const socket = io('http://theapp.dufullstack.fr')
const socket = io()
const app = feathers()
app.configure(feathers.socketio(socket))
export default app
