module.exports = {

  devServer: {
    // see: https://cli.vuejs.org/config/#devserver-proxy)

    proxy: {
      '/socket.io/.*': {
        target: 'http://localhost:3035/'
      }
    }

  }
}
